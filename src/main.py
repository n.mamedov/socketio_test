from flask import Flask
from flask_socketio import SocketIO, join_room, leave_room
import json
from jose import JWTError, jwt
from pydantic import BaseSettings, Field
from src.database.models.chat import Chat, UsersChat, Message
from src.database.models.practice import Practice
from src.database.models.auth import User
from src.database.deps import SessionLocal
from sqlalchemy import desc
app = Flask(__name__)

app.config['SECRET_KEY'] = 'secret!'
socketio = SocketIO(app, cors_allowed_origins="*")

app.debug = True

db = SessionLocal()


class Settings(BaseSettings):
    jwt_secret_key: str = Field(..., env='JWT_SECRET_KEY')
    access_token_expire_minutes: int = Field(..., env='ACCESS_TOKEN_EXPIRE_MINUTES')
    refresh_token_expire_minutes: int = Field(..., env='REFRESH_TOKEN_EXPIRE_DAYS')
    algorithm: str = Field(..., env='ALGORITHM')


settings = Settings()


def read_carts(chat_id: int):
    chat = db.query(Chat).filter(Chat.id == chat_id).one()
    if chat.practice_id is not None:
        practice = db.query(Practice).filter(Practice.id == chat.practice_id).one()
        return json.dumps({'card': {'id': practice.id, 'start_time': practice.start_time,
                                    'type': practice.practice_type, 'paid': practice.paid, 'guru': practice.guru_id}})


def check_access_and_get_user(token: str) -> int or None:
    try:
        payload = jwt.decode(token, settings.jwt_secret_key, algorithms=[settings.algorithm])
        return payload.get("user")
    except JWTError:
        return None


@socketio.on('connect', namespace='')
def test_connect():
    socketio.emit('my response', {'data': 'Connected'})


@socketio.on('join')
def on_join(data):
    data = json.loads(data)
    user=check_access_and_get_user(data['token'])
    user_name=db.query(User).filter(User.id == user).first()
    username = user_name.name
    room = data['room']
    join_room(room)
    messages = db.query(Message).filter(Message.chat_id == room).order_by(desc(Message.date)).all()
    all_messages = {'history': []}
    for message in messages:
        json_message = json.dumps(
            {'id': message.id, 'message': message.message, "user": message.user_id, "data": message.date}, indent=4,
            sort_keys=True, default=str)
        all_messages['history'].append(json_message)
    socketio.send(json.dumps(all_messages), to=room)
    socketio.send(read_carts(room), to=room)
    socketio.emit(username + ' has entered the room.', to=room)


@socketio.on('message')
def handle_message(message):
    print(message)
    user = check_access_and_get_user(message['token'])
    message = json.loads(message)
    new_message = Message(
        user_id=user,
        chat_id=message['room'],
        message=message['data']
    )

    db.add(message)
    db.commit()
    new_message = {'id': new_message.id, 'message': new_message.message, "user": new_message.user_id, "data": new_message.date}
    socketio.send(json.dumps(new_message), namespace='', to=message['room'])




@socketio.on('disconnect', namespace='')
def test_disconnect():
    print('Client disconnected')


if __name__ == '__main__':
    socketio.run(app, host='0.0.0.0', port='5000', debug=True)