from sqlalchemy import Column, Integer, String, ForeignKey
from sqlalchemy.orm import relationship

from app.database import deps as database_deps


class Guru(database_deps.Base):
    __tablename__ = "gurus"

    id = Column(Integer, primary_key=True, index=True)
    name = Column(String)
    surname = Column(String)
    description = Column(String)
    image = Column(String)
    specialization = Column(String)
    individual_class_price = Column(Integer)

    challenges = relationship("Challenge", cascade="all, delete")
    projects = relationship("Project", cascade="all, delete")
    practices = relationship("Practice", cascade="all, delete")


class Project(database_deps.Base):
    __tablename__ = "projects"

    id = Column(Integer, primary_key=True, index=True)
    guru_id = Column(Integer, ForeignKey("gurus.id"), index=True)
    description = Column(String)
    image = Column(String)
    link = Column(String)
