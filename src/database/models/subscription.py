from sqlalchemy import Column, Integer, String, ForeignKey, DateTime
from app.database import deps as database_deps
from app.database.models.auth import User


class Subscroption(database_deps.Base):
    __tablename__ = "subscription"

    id = Column(Integer, primary_key=True)
    user_id = Column(Integer, ForeignKey(User.id), index=True)
    start_time = Column(DateTime)
    type = Column(String, nullable=False)
    end_time = Column(DateTime)


