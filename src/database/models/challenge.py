from sqlalchemy import Column, Integer, String, DateTime, Enum, ForeignKey, Boolean, Index
from sqlalchemy.orm import relationship

from app.database import deps as database_deps
from app.database.schemas import challenge as challenge_schemas
from app.database.models.auth import User
from app.database.models.guru import Guru


class Challenge(database_deps.Base):
    __tablename__ = "challenges"

    id = Column(Integer, primary_key=True, index=True)
    created_at = Column(DateTime)
    starts_at = Column(DateTime)
    ends_at = Column(DateTime)
    guru_id = Column(Integer, ForeignKey(Guru.id))
    preview = Column(String)
    title = Column(String)
    description = Column(String)
    duration = Column(Integer)
    participants_max = Column(Integer)
    participants_left = Column(Integer)
    time_required_min = Column(Integer)
    time_required_max = Column(Integer)
    category = Column(Enum(challenge_schemas.ChallengeCategories), index=True)
    image = Column(String)

    user_challenges = relationship("UserChallenge", cascade="all, delete")
    challenge_meditations = relationship("ChallengeMeditation", cascade="all, delete")
    challenge_onlines = relationship("ChallengeOnline", cascade="all, delete")
    user_tasks = relationship("UserTask", cascade="all, delete")


class UserChallenge(database_deps.Base):
    __tablename__ = "user_challenges"
    __table_args__ = (Index('UserChallenge-user_id-challenge_id', "user_id", "challenge_id"),)

    id = Column(Integer, primary_key=True, index=True)
    user_id = Column(Integer, ForeignKey(User.id))
    challenge_id = Column(Integer, ForeignKey(Challenge.id))
    completed = Column(Boolean, default=False)
