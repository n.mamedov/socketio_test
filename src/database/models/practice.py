from sqlalchemy import Column, Integer, ForeignKey, DateTime, Boolean, Enum

from app.database import deps as database_deps
from app.database.models.auth import User
from app.database.models.guru import Guru
from app.database.models.studio import Studio
from app.database.models.task import Online
from app.database.models.teacher import Teacher
from app.database.schemas import practice as practice_schemas


class Practice(database_deps.Base):
    __tablename__ = "practices"

    id = Column(Integer, primary_key=True, index=True)
    user_id = Column(Integer, ForeignKey(User.id), index=True)
    start_time = Column(DateTime)
    paid = Column(Boolean, default=False)
    master_type = Column(Enum(practice_schemas.MasterType))
    guru_id = Column(Integer, ForeignKey(Guru.id), index=True)
    teacher_id = Column(Integer, ForeignKey(Teacher.id), index=True)
    practice_type = Column(Enum(practice_schemas.PracticeType))
    online_id = Column(Integer, ForeignKey(Online.id))      # TODO тут тоже есть start_time
    studio_id = Column(Integer, ForeignKey(Studio.id))
