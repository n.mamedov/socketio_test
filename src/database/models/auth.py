from sqlalchemy import Column, ForeignKey, Integer, String
from sqlalchemy.orm import relationship

from app.database import deps as database_deps


class Token(database_deps.Base):
    __tablename__ = "tokens"

    id = Column(Integer, primary_key=True, index=True)
    access = Column(String)
    refresh = Column(String)
    user_id = Column(Integer, ForeignKey("users.id"), index=True)


class User(database_deps.Base):
    __tablename__ = "users"

    id = Column(Integer, primary_key=True, index=True)
    hex_uuid = Column(String, unique=True, index=True, nullable=True)
    email = Column(String, unique=True, index=True, nullable=True)
    form_of_address = Column(String)
    name = Column(String)
    sub = Column(String, unique=True, index=True, nullable=True)

    tokens = relationship("Token", cascade="all, delete")
    settings = relationship("Setting", cascade="all, delete")
