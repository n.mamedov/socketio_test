from sqlalchemy import Column, Integer, String, ARRAY
from sqlalchemy.orm import relationship

from app.database import deps as database_deps


class Studio(database_deps.Base):
    __tablename__ = "studios"

    id = Column(Integer, primary_key=True, index=True)
    name = Column(String)
    point = Column(String)
    phone = Column(String)
    images = Column(ARRAY(String))

    practices = relationship("Practice", cascade="all, delete")
