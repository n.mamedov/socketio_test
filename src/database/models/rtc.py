from sqlalchemy import Column, Integer, Boolean, String, ForeignKey, DateTime
from app.database import deps as database_deps
from app.database.models.auth import User


class VideoRoom(database_deps.Base):
    __tablename__ = "video_rooms"

    id = Column(Integer, primary_key=True)
    hex_uuid = Column(String, unique=True, index=True, nullable=False)
    closed = Column(Boolean, default=False)
    master_user_id = Column(Integer, ForeignKey(User.id), index=True)
    student_user_id = Column(Integer, ForeignKey(User.id), index=True, nullable=True)
    start_time = Column(DateTime)
    end_time = Column(DateTime)
    session_id = Column(String, nullable=False)
