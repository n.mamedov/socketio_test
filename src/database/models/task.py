from sqlalchemy import Column, Integer, String, DateTime, ForeignKey, Boolean, Index
from sqlalchemy.orm import relationship

from app.database import deps as database_deps
from app.database.models.challenge import Challenge
from app.internal.auth import User


class Meditation(database_deps.Base):
    __tablename__ = "meditations"

    id = Column(Integer, primary_key=True, index=True)
    uuid = Column(String, index=True, unique=True)
    title = Column(String)
    start_time = Column(DateTime)
    audio = Column(String)
    duration = Column(Integer)
    tag = Column(String)
    image = Column(String)

    challenges = relationship("ChallengeMeditation", cascade="all, delete")
    user_tasks = relationship("UserTask", cascade="all, delete")


class UserMeditation(database_deps.Base):
    __tablename__ = "user_meditations"

    id = Column(Integer, primary_key=True, index=True)
    user_id = Column(Integer, ForeignKey(User.id))
    pinned = Column(Boolean, default=False)
    meditation_uuid = Column(String, ForeignKey(Meditation.uuid))
    click_counter = Column(Integer, default=0)

    __table_args__ = (
        Index('UserMeditation-user_id-pinned-click_counter', user_id, pinned, click_counter.desc()),
        Index('UserMeditation-user_id-meditation_uuid', user_id, meditation_uuid),
    )


class ChallengeMeditation(database_deps.Base):
    __tablename__ = "challenge_meditations"

    id = Column(Integer, primary_key=True, index=True)
    challenge_id = Column(Integer, ForeignKey(Challenge.id), index=True)
    meditation_uuid = Column(String, ForeignKey(Meditation.uuid))


class Online(database_deps.Base):
    __tablename__ = "onlines"

    id = Column(Integer, primary_key=True, index=True)
    uuid = Column(String, index=True, unique=True)
    title = Column(String)
    start_time = Column(DateTime)
    chat_id = Column(Integer)
    description = Column(String)
    tag = Column(String)

    challenges = relationship("ChallengeOnline", cascade="all, delete")
    user_tasks = relationship("UserTask", cascade="all, delete")
    practices = relationship("Practice", cascade="all, delete")


class ChallengeOnline(database_deps.Base):
    __tablename__ = "challenge_onlines"

    id = Column(Integer, primary_key=True, index=True)
    challenge_id = Column(Integer, ForeignKey(Challenge.id), index=True)
    online_uuid = Column(String, ForeignKey(Online.uuid))


class UserTask(database_deps.Base):
    __tablename__ = "user_tasks"
    __table_args__ = (Index('UserTask-user_id-challenge_id', "user_id", "challenge_id"),)

    id = Column(Integer, primary_key=True, index=True)
    user_id = Column(Integer, ForeignKey(User.id))
    challenge_id = Column(Integer, ForeignKey(Challenge.id))
    meditation_uuid = Column(String, ForeignKey(Meditation.uuid))
    online_uuid = Column(String, ForeignKey(Online.uuid))
    completed = Column(Boolean, default=False)
