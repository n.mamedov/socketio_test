from sqlalchemy import Column, ForeignKey, Integer, Boolean
from app.database import deps as database_deps
from app.database.models.auth import User


class Setting(database_deps.Base):
    __tablename__ = "settings"

    id = Column(Integer, primary_key=True)
    notify_about_classes = Column(Boolean, default=True)
    notify_about_purchases = Column(Boolean, default=True)
    min_purchase_for_notify = Column(Integer, default=1000)
    user_id = Column(Integer, ForeignKey(User.id), index=True)
