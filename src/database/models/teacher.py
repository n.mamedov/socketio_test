from sqlalchemy import Column, Integer, String
from sqlalchemy.orm import relationship

from app.database import deps as database_deps


class Teacher(database_deps.Base):
    __tablename__ = "teachers"

    id = Column(Integer, primary_key=True, index=True)
    name = Column(String)
    surname = Column(String)
    description = Column(String)
    image = Column(String)
    individual_class_price = Column(Integer)

    practices = relationship("Practice", cascade="all, delete")
