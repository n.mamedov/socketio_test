from sqlalchemy import Column, ForeignKey, Integer, String, DateTime
from app.database import deps as database_deps
from sqlalchemy.sql import func
from app.internal.auth import User


class Chat(database_deps.Base):
    __tablename__ = "chat"

    id = Column(Integer, primary_key=True, index=True)
    type = Column(Integer)


class UsersChat(database_deps.Base):
    __tablename__ = "userschat"

    id = Column(Integer, primary_key=True, index=True)
    user_id = Column(Integer, ForeignKey(User.id))
    chat_id = Column(Integer, ForeignKey(Chat.id))


class Message(database_deps.Base):
    __tablename__ = "message"

    id = Column(Integer, primary_key=True, index=True)
    user_id = Column(Integer, ForeignKey(User.id))
    chat_id = Column(Integer, ForeignKey(Chat.id))
    message = Column(String)
    date = Column(DateTime(timezone=True), server_default=func.now())
