from pydantic import BaseModel


class StudioBase(BaseModel):
    name: str
    point: str
    phone: str
    images: list[str]


class StudioCreate(StudioBase):
    pass


class StudioRead(StudioBase):
    id: int

    class Config:
        orm_mode = True
