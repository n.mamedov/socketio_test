import enum
import datetime

from typing import Optional
from pydantic import BaseModel


class ChallengeCategories(str, enum.Enum):
    mindfullness_training = "mindfullness_training"
    getting_rid_of_excess = "getting_rid_of_excess"
    working_out_techniques = "working_out_techniques"


class ChallengeBase(BaseModel):
    starts_at: datetime.datetime
    ends_at: datetime.datetime
    guru_id: int
    preview: str
    title: str
    description: str
    duration: int
    image: str
    participants_max: int
    time_required_min: Optional[int]
    time_required_max: Optional[int]
    category: ChallengeCategories


class ChallengeCreate(ChallengeBase):
    class Config:
        orm_mode = True


class ChallengeRead(ChallengeBase):
    id: int
    participants_left: int
    active: bool
    status: dict
    tags: list

    class Config:
        orm_mode = True
