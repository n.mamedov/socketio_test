import enum
import datetime
from typing import Optional

from pydantic import BaseModel, validator


class MasterType(str, enum.Enum):
    guru = "guru"
    teacher = "teacher"


class PracticeType(str, enum.Enum):
    online = "online"
    studio = "studio"


class PracticeBase(BaseModel):
    user_id: int
    start_time: datetime.datetime
    paid: bool
    master_type: MasterType
    guru_id: Optional[int]
    teacher_id: Optional[int]
    practice_type: PracticeType
    online_id: Optional[int]
    studio_id: Optional[int]

    @validator('guru_id')
    def validate_guru_id(cls, v, values):
        if values['master_type'] == MasterType.guru.value and v is None:
            raise ValueError('guru_id must be integer when master_type is guru')
        elif values['master_type'] == MasterType.teacher.value and v is not None:
            raise ValueError('guru_id must be null when master_type is teacher')
        return v

    @validator('teacher_id')
    def validate_teacher_id(cls, v, values):
        if values['master_type'] == MasterType.teacher.value and v is None:
            raise ValueError('teacher_id must be integer when master_type is teacher')
        elif values['master_type'] == MasterType.guru.value and v is not None:
            raise ValueError('teacher_id must be null when master_type is guru')
        return v

    @validator('online_id')
    def validate_online_id(cls, v, values):
        if values['practice_type'] == PracticeType.online.value and v is None:
            raise ValueError('online_id must be integer when practice_type is online')
        elif values['practice_type'] == PracticeType.studio.value and v is not None:
            raise ValueError('online_id must be null when practice_type is studio')
        return v

    @validator('studio_id')
    def validate_studio_id(cls, v, values):
        if values['practice_type'] == PracticeType.studio.value and v is None:
            raise ValueError('studio_id must be integer when practice_type is studio')
        elif values['practice_type'] == PracticeType.online.value and v is not None:
            raise ValueError('studio_id must be null when practice_type is online')
        return v


class PracticeCreate(PracticeBase):
    pass


class PracticeRead(PracticeBase):
    id: int

    class Config:
        orm_mode = True
