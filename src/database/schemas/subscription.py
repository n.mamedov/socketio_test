from pydantic import BaseModel


class Subscription(BaseModel):
    responseBodyV2: str
    notificationType: str
    subtype: str
