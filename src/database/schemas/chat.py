from pydantic import BaseModel
from typing import List


class ChatBase(BaseModel):
    chat_type: int
    users: List[int] = None


class MessageBase(BaseModel):
    message: str
    sender: int


class ChatRead(BaseModel):
    chat_type: int
    user_id: int
