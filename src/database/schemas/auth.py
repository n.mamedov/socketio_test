from typing import Optional

from pydantic import BaseModel
from fastapi.openapi.models import Schema


class Uuid(BaseModel):
    hex_uuid: str


class Token(BaseModel):
    token: str


class UserApple(BaseModel):
    form_of_address: Optional[str]
    name: Optional[str]
    token: str


class UserBase(BaseModel):
    form_of_address: str
    name: str


class User(UserBase):
    hex_uuid: Optional[str] = None

    class Config:
        orm_mode = True


class Headers(BaseModel):
    access: str = Schema(
        type='string',
        description='Access token',
    )
    refresh: str = Schema(
        type='string',
        description='Refresh token',
    )


class AuthResponseSchema(BaseModel):
    headers: Headers
    body: Uuid
