from typing import Optional
from pydantic import BaseModel, validator


class SettingBase(BaseModel):
    notify_about_classes: bool
    notify_about_purchases: bool
    min_purchase_for_notify: Optional[int]

    @validator('min_purchase_for_notify')
    def validate_guru_id(cls, v, values):
        if values['notify_about_purchases'] and v is None:
            raise ValueError('min_purchase_for_notify must be integer when notify_about_purchases is true')
        elif not values['notify_about_purchases'] and v is not None:
            raise ValueError('min_purchase_for_notify must be null when notify_about_purchases is false')
        return v


class Setting(SettingBase):
    class Config:
        orm_mode = True
