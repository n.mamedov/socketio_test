from pydantic import BaseModel


class Price(BaseModel):
    price: int


class Message(BaseModel):
    detail: str


class VideoRoom(BaseModel):
    hex_uuid: str
