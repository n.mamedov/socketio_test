from typing import Optional
from pydantic import BaseModel


class TeacherBase(BaseModel):
    name: str
    surname: str
    description: str
    image: str
    individual_class_price: Optional[int]


class TeacherCreate(TeacherBase):
    pass


class TeacherRead(TeacherBase):
    id: int

    class Config:
        orm_mode = True
