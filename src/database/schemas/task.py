import datetime

from typing import Optional
from pydantic import BaseModel


class OnlineBase(BaseModel):
    title: str
    start_time: datetime.datetime
    chat_id: int
    description: str
    tag: str


class OnlineCreate(OnlineBase):
    challenge_id: int

    class Config:
        orm_mode = True


class OnlineRead(OnlineBase):
    id: int
    uuid: str

    class Config:
        orm_mode = True


class MeditationBase(BaseModel):
    title: str
    start_time: datetime.datetime
    audio: str
    tag: str
    image: str


class MeditationCreate(MeditationBase):
    challenge_id: int

    class Config:
        orm_mode = True


class MeditationRead(MeditationBase):
    id: int
    uuid: str
    pinned: Optional[bool]
    duration: int

    class Config:
        orm_mode = True


class TaskRead(BaseModel):
    title: str
    start_time: datetime.datetime
    tag: str
    completed: Optional[bool]
    active: bool
    audio: Optional[str]
    duration: Optional[int]
    image: Optional[str]
    chat_id: Optional[int]
    description: Optional[str]
    meditation_id: Optional[int]
    online_id: Optional[int]
    meditation_uuid: Optional[str]
    online_uuid: Optional[str]

    class Config:
        orm_mode = True
