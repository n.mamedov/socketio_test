from typing import Optional
from pydantic import BaseModel


class GuruBase(BaseModel):
    name: str
    surname: str
    description: str
    image: str
    specialization: str
    individual_class_price: Optional[int]


class GuruCreate(GuruBase):
    pass


class GuruRead(GuruBase):
    id: int
    active_challenges: Optional[int]

    class Config:
        orm_mode = True


class ProjectBase(BaseModel):
    guru_id: int
    description: str
    image: str
    link: str


class ProjectCreate(ProjectBase):
    pass


class ProjectRead(ProjectBase):
    id: int

    class Config:
        orm_mode = True
